@extends('layouts.app')

@section('head')
<title>Clients</title>
@endsection

@section('content')

<div class="container pt-2">

    <div class="row">
        <div class="col-md-2 ml-1">
            <a class="btn btn-water" href="{{ URL::action('ClientController@create') }}">New Client</a>
        </div>
    </div>

    <div class="row" id="order">
        @if (count($clients) > 0)
        @foreach ($clients as $cli)
        <div class="col-md-2 card mx-3 my-3 pb-4 border-air" data-toggle="modal">
            <img src="{{ $cli->logo }}" class="card-img-top img-thumbnails mx-auto py-4" style="max-height: 100px; max-width:180px;" alt="Logo">
            <div class="card-body">
                <p class="text-center"><strong>{{ $cli->business_name }}</strong></p>
                <p class="text-center">{{ $cli->slogan }}</p>
                <div class="row bottom-fixed">
                    <div class="col mx-auto">
                        <a id="show{{ $cli->id }}" name="{{ $cli->id }}" class="show btn btn-water btn-sm text-white" data-toggle="modal" data-target="#showClientModal{{ $cli->id }}"> Show </a>
                        <a id="delete{{ $cli->id }}" name="{{ $cli->id }}" class="btn btn-danger btn-sm text-white" data-toggle="modal" data-target="#deleteClientModal{{ $cli->id }}"> Delete </a>
                    </div>
                </div>

            </div>
        </div>
        <!-- modal show -->
        <div class="modal fade" id="showClientModal{{ $cli->id }}" tabindex="-1" role="dialog" aria-labelledby="showClientModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-lightwater">
                        <h5 class="modal-title strong text-center text-white" id="showClientModalLabel">{{ $cli->business_name }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <div id="showModal{{ $cli->id }}">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <a id="delete2{{ $cli->id }}" name="{{ $cli->id }}" class="btn btn-danger text-white" data-toggle="modal" data-target="#deleteClientModal{{ $cli->id }}"> Delete </a>
                        <button type="button" class="btn btn-water" data-dismiss="modal"> Close </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal delete -->
        <div class="modal fade" id="deleteClientModal{{ $cli->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteClientModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h5 class="modal-title strong text-center text-white" id="showClientModalLabel">{{ $cli->business_name }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <div id="showModal{{ $cli->id }}">
                            <strong>Are you sure you want to delete {{ $cli->business_name }}?</strong>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="{{ URL::action('ClientController@destroy', $cli->id) }}" class="btn btn-danger btn-delete" data-id="{{ $cli->id }}"> Delete </a>
                        <button type="button" class="btn btn-water" data-dismiss="modal"> Cancel </button>
                    </div>
                </div>
            </div>
        </div>
        @endforeach

        @else
        <p class="ml-5">No client</p>
        @endif

    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function() {

        var _token = $('input[name="_token"]').val();

        function show_client(id = '') {
            $.ajax({
                url: "http://localhost:8000/client/" + id,
                method: "GET",
                data: {
                    id: id,
                    _token: _token
                },
                dataType: "json",
                success: function(data) {
                    var output = '';

                    output += '<p> Referent Name: ' + data[0].referent_name + ' ' + data[0].business_surname + '</p>';
                    output += '<a class="text-black u" href="mailto:' + data[0].referent_email + '"> Email: ' + data[0].referent_email + '</a>';
                    output += '<p class="my-3"> SSID: ' + data[0].SSID + '€' + '</p>';
                    output += '<a class="text-black u" href="mailto:' + data[0].PEC + '"> P.E.C.: ' + data[0].PEC + '</a>';
                    output += '<p class="my-3"> Partita Iva: ' + data[0].P_IVA + '</p>';

                    $('#showModal' + id).html(output);
                }
            })
        }

        $('.show').click(function() {
            var id = $(this).attr('name');
            show_client(id);
        });

    });
</script>
@endsection