@extends('layouts.app')

@section('head')
<title>Client</title>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <p>id: {{ $client->id }}</p>
            <p>name: {{ $client->business_name }} </p>
            <p>referent name{{ $client->referent_name }}</p>
            <p>referent surname{{ $client->business_surname }}</p>
            <p>referent email{{ $client->referent_email }}</p>
            <p>SSID{{ $client->SSID }}</p>
            <p>PEC{{ $client->PEC }}</p>
            <p>P. IVA{{ $client->P_IVA }}</p>
            <p><img src="{{ $client->logo }}"></p>
            <p>{{ $client->slogan }}</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
                <a href="{{ URL::action('ClientController@destroy', $client->id) }}" class="btn btn-danger btn-sm btn-delete" data-id="{{ $client->id }}"> Delete </a>
        </div>
    </div>
@endsection