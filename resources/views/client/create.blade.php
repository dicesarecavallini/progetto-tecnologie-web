@extends('layouts.app')

@section('head')
@endsection

@section('content')
<div class="container">
  <div class="row">

    <div class="col-md-3">
      <a class="btn btn-water my-1" href="{{ URL::action('ClientController@index') }}" role="button">Back to all Clients</a>
    </div>

    <div class="col-md-6">

      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="card border-grey">
        <div class="card-header bg-air font-weight-bold">{{ __('Add a New Client') }}</div>

        <div class="card-body px-5">

          <form action="{{ URL::action('ClientController@store') }}" method="POST">
            {{ csrf_field() }}

            <div class="form-group row">
              <label for="business_name">Business Name</label>
              <input class="form-control" type="text" name="business_name" value="{{ old('business_name') }}" />
            </div>

            @error('business_name')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror

            <div class="form-group row">
              <label for="referent_name">Referent Name</label>
              <input class="form-control" type="text" name="referent_name" value="{{ old('referent_name') }}" />
            </div>

            @error('referent_name')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror

            <div class="form-group row">
              <label for="business_surname">Referent Surname</label>
              <input class="form-control" type="text" name="business_surname" value="{{ old('business_surname') }}" />
            </div>

            @error('business_name')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror

            <div class="form-group row">
              <label for="referent_email">Referent eMail</label>
              <input class="form-control" type="text" name="referent_email" value="{{ old('referent_email') }}" />
            </div>

            @error('referent_email')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror

            <div class="form-group row">
              <label for="SSID">SSID</label>
              <input class="form-control" type="text" name="SSID" value="{{ old('SSID') }}" />
            </div>

            @error('SSID')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror

            <div class="form-group row">
              <label for="PEC">P.E.C.</label>
              <input class="form-control" type="text" name="PEC" value="{{ old('PEC') }}" />
            </div>

            @error('PEC')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror

            <div class="form-group row">
              <label for="P_IVA">VAT Number</label>
              <input class="form-control" type="text" name="P_IVA" value="{{ old('P_IVA') }}" />
            </div>

            @error('P_IVA')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror

            <div class="form-group row">
              <label for="logo">Logo's Link</label>
              <input class="form-control" type="text" name="logo" value="{{ old('logo') }}" />
            </div>

            @error('logo')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror

            <div class="form-group row">
              <label for="slogan">Slogan</label>
              <input class="form-control" type="text" name="slogan" value="{{ old('slogan') }}" />
            </div>

            @error('slogan')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror

            <div class="row justify-content-center">
              <input class="btn btn-water" type="submit" value="Add Client">
            </div>
          </form>

        </div>
      </div>
    </div>

  </div>
</div>
</div>
@endsection