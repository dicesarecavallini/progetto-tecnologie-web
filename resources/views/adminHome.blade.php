@extends('layouts.app')

@section('content')
<div class="container">
    <h1 class="h1 text-center strong my-2">Hi {{ Auth::user()->name }}!</h1>

    <div class="row justify-content-center">
        <div class="col-md-4 col-xs-12">
            <div class="card border-black my-4">
                <div class="h4 card-header bg-lightwater text-center text-white">Most Recent Project</div>

                <div class="card-body bg-cloud text-center">
                    @foreach($projects as $pro)
                    <a href="{{ URL::action('ProjectController@show', $pro->id) }}" class="text-black font-500">{{ $pro->name}}</a>
                    <p>
                        {{date('d/m/Y', strtotime($pro->created_at))}}
                    </p>
                    <hr>
                    @endforeach
                    <a href="{{ URL::action('ProjectController@index') }}" class="justify-content-right text-black u mb-1">Show all Projects </a>
                </div>

            </div>
        </div>
        <div class="col-md-4 col-xs-12 my-auto">

            <div class="row justify-content-center py-4">
                <div class="col-md-5 col-sm-2 my-3">
                    <a role="button" href="{{ URL::action('ClientController@create') }}" class="btn btn-air bg-air btn-lg text-dark mx-auto my-2">Add a Client</a>
                </div>
                <div class="col-md-5 col-sm-2 my-3">
                    <a role="button" href="{{ URL::action('ProjectController@create') }}" class="btn btn-air bg-air btn-lg text-dark mx-auto my-2">Add a Project</a>
                </div>
                <div class="col-md-5 col-sm-2 my-3">
                    <a role="button" href="{{ URL::action('UserController@create') }}" class="btn btn-air btn-lg text-dark mx-auto my-2">Add an Employee</a>
                </div>
                <div class="col-md-5 col-sm-2 my-3">
                    <a role="button" href="{{ URL::action('TaskController@index') }}" class="btn btn-air btn-lg text-dark mx-auto my-2">Add a Task</a>
                </div>
            </div>



        </div>
        <div class="col-md-4 col-xs-12">
            <div class="card border-black my-4">
                <div class="h4 card-header bg-lightwater text-center text-white">Employees With Less Tasks</div>

                <div class="card-body bg-cloud text-center">
                    @if(count($noTasks) != 0)
                        @foreach($noTasks as $t)
                        <h6>{{ $t->name}} {{ $t->surname}}</h6>
                        <p>N. jobs: 0</p>
                        <hr>
                        @endforeach
                    @endif

                    @if(count($users) != 0)
                        @foreach($users as $u)
                        <h6>{{ $u->name}} {{ $u->surname}}</h6>
                        <p>N. jobs: {{ $u->n}}</p>
                        <hr>
                        @endforeach
                    @endif
                    <a href="{{ URL::action('UserController@index') }}" class="justify-content-right text-black u mb-1">Show all Employees </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection