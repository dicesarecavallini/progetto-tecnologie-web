@extends('layouts.app')

@section('head')
<title>Tasks</title>
@endsection

@section('content')

<div class="container mt-3">

    <div class="row">
        <div class="col-md-3">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="card border-grey mb-3">
                <div class="card-header bg-air font-weight-bold">Add a New Task</div>
                <div class="card-body px-5">

                    <form action="http://localhost:8000/task/store/" id="addTask" method="POST">
                        {{ csrf_field() }}

                        <div class="form-group row">
                            <label for="id_u">User</label>
                            <select class="form-control" name="id_u">
                                @foreach ($users as $user)
                                <option class="user" value="{{ $user->id }}">{{ $user->name }} {{ $user->surname }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group row">
                            <label for="id_p">Project</label>
                            <select class="form-control" name="id_p">
                                @foreach ($projects as $project)
                                <option class="project" value="{{ $project->id }}">{{ $project->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="row justify-content-center">
                            <input class="addTask btn btn-water" type="submit" value="Add Task">
                        </div>
                    </form>

                </div>
            </div>
        </div>


        <div class="col-md-9">
            @if (count($tasks) > 0)
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th scope="col" class="border-air bg-water-40">#</th>
                                <th scope="col" class="border-air bg-water-40">User</th>
                                <th scope="col" class="border-air bg-water-40">Project</th>
                                <th scope="col" class="border-air bg-water-40">Hours Completed</th>
                                <th scope="col" class="border-air bg-water-40">Delete</th>
                            </tr>
                        </thead>
                        <tbody class="tabella">
                            @foreach ($tasks as $task)
                            <tr>
                                <td>{{ $task->id }}</td>
                                <td>{{ $task->user->name}} {{ $task->user->surname }}</td>
                                <td>{{ $task->project->name }}</td>
                                <td>
                                    @if($task->hours_completed == 'undefined')
                                    0
                                    @elseif($task->hours_completed == NULL)
                                    0
                                    @else
                                    {{ $task->hours_completed }}
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-danger text-white btn-sm btn-delete" data-id="{{ $task->id }}"> Delete </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @else
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">User</th>
                        <th scope="col">Project</th>
                        <th scope="col">Hours Completed</th>
                        <th scope="col">Delete</th>
                    </tr>
                </thead>
                <tbody class="tabella">
                    <tr>
                        <td>
                            <p class="noTasks">No tasks</p>
                        </td>
                    </tr>
                </tbody>
            </table>
            @endif
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        var _token = $('input[name="_token"]').val();

        $('#addTask').on('submit', function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                url: $(this).attr('action'),
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {

                    var output = '';
                    output += '<tr><td>' + data[0].id + '</td>';
                    output += '<td>' + data[0].user_name + ' ' + data[0].user_surname + '</td>';
                    output += '<td>' + data[0].project_name + '</td>';
                    output += '<td>' + data[0].hour_completed + '</td>';
                    output += '<td><a class="btn btn-danger text-white btn-sm btn-delete" data-id="' + data[0].id + '"> Delete </a></td></tr>';

                    $('.tabella').append(output);
                    $('.noTasks').parents('tr').hide();

                }
            });
        });

        $('.tabella').on('click', '.btn-delete', function() {
            var button = $(this);
            var id = $(button).attr('data-id');
            $.ajax({
                url: "/task/" + id + "/delete",
                type: "GET",
                dataType: "json",
                success: function(data) {
                    $(button).parents('tr').fadeOut();
                }
            });
        });

    });
</script>
@endsection