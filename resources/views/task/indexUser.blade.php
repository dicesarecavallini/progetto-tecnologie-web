@extends('layouts.app')

@section('head')
<title>Tasks</title>
@endsection

@section('content')

<div class="container mt-3">

<div class="row">    

    <div class="col-md-3">
        <a class="btn btn-water" href="{{ URL::action('UserController@index') }}" role="button">Back to all Employees</a>
    </div>
    
    <div class="col-md-12">
        @if (count($tasks) > 0)
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">User</th>
                        <th scope="col">Project</th>
                        <th scope="col">Hours Completed</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tasks as $task)
                    <tr>
                        <td>{{ $task->id }}</td>
                        <td>{{ $task->user->name}} {{ $task->user->surname }}</td>
                        <td>{{ $task->project->name }}</td>
                        <td>{{ $task->hours_completed }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        @else 
            <p>No tasks</p>
        @endif
    </div>        
</div>
@endsection