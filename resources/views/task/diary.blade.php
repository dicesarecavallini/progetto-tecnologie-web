@extends('layouts.datepicker')

@section('head')
@endsection

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row mt-2 mb-4 justify-content-between">
                <div class="col-md-3 py-auto my-2">
                    <a class="btn btn-water" name="addSchedaOre" href="{{ URL::action('CardController@insert') }}" id="addSchedaOre" class="btn btn-water">Add Hours Card</a>
                </div>

                <div class="col-md-3"></div>

                <div class="col-md-3 my-auto py-auto">
                    <input type="month" id="from_date" name="from_date" class="input-group input-daterange" value="{{$y}}">
                </div>

                <div class="col-md-2 my-auto py-auto align-self-end">
                    <button type="button" name="filter" id="filter" class="btn btn-water btn-sm my-1">Filter</button>
                    <button type="button" name="refresh" id="refresh" class="btn btn-cloud btn-sm my-1">Refresh</button>
                </div>

            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th scope="col" class="border-air bg-water-40">Project</th>
                                <th scope="col" class="border-air bg-water-40">Hours Completed</th>
                                <th scope="col" class="border-air bg-water-40">Pay per Hour</th>
                                <th scope="col" class="border-air bg-water-40">Start Date</th>
                                <th scope="col" class="border-air bg-water-40">End Date</th>
                                <th scope="col" class="border-air bg-water-40">Finished</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    {{ csrf_field() }}
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function() {

        var date = new Date();

        /*  $('.input-daterange').datepicker({
              todayBtn: 'linked',
              format: 'yyyy-mm',
              autoclose: true
          }); */

        var _token = $('input[name="_token"]').val();

        fetch_data();

        function fetch_data(from_date = '') {
            $.ajax({
                url: "{{ route('daterange.fetch_data') }}",
                method: "POST",
                data: {
                    from_date: from_date,
                    _token: _token
                },
                dataType: "json",
                success: function(data) {
                    var output = '';
                    var completed = 'No';
                    $('#total_records').text(data.length);
                    for (var count = 0; count < data.length; count++) {
                        if (data[count].end_date < date.toJSON()) {
                            completed = 'Yes';
                        }
                        data1 = (new Date(data[count].start_date));
                        data2 = (new Date(data[count].end_date));

                        
                        
                        output += '<tr>';
                        output += '<td>' + data[count].name + '</td>';
                        output += '<td>' + data[count].hours + '</td>';
                        output += '<td>' + data[count].hourly_cost + '€' + '</td>';
                        output += '<td>' + data1.getDate() + '/' +data1.getMonth() + '/' + data1.getFullYear() + '</td>';
                        output += '<td>' + data2.getDate() + '/' +data2.getMonth() + '/' + data2.getFullYear() + '</td>';
                        output += '<td>' + completed + '</td></tr>';
                    }

                    console.log(data[count - 1]);
                    console.log(date.toJSON());

                    $('tbody').html(output);
                }
            })
        }

        var d = $('#from_date').val();

        $('#filter').click(function() {
            var from_date = $('#from_date').val();
            console.log(from_date);
            if (from_date != '') {
                fetch_data(from_date);
            } else {
                alert('Date is required');
            }
        });

        $('#refresh').click(function() {
            $('#from_date').val(d);
            fetch_data();
        });

    });
</script>
@endsection