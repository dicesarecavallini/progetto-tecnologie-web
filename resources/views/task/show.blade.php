@extends('layouts.app')

@section('head')
<title>Tasks</title>
@endsection

@section('content')
<h1>Tasks</h1>

<div class="row">
    <div class="col-md-12">
        <a class="btn btn-primary float-md-right" href="{{ URL::action('ProjectController@create') }}" >new task</a>
    </div>
</div>
<br />
<div class="row">
    <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">User</th>
                        <th scope="col">Project</th>
                        <th scope="col">Hours Completed</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tasks as $task)
                    <tr>
                        <td>{{ $task->id }}</td>
                        <td>{{ $task->id_u }}</td>
                        <td>{{ $task->id_p }}</td>
                        <td>{{ $task->huors_completed }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        
    </div>        
</div>
@endsection