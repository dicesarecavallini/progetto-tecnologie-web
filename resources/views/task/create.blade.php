@extends('layouts.app')

@section('head')
@endsection

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-3">
            <a class="btn btn-water my-1" href="{{ URL::action('TaskController@index') }}" role="button">Back to all Tasks</a>
        </div>

        <div class="col-md-6">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="card border-grey">
                <div class="card-header bg-air font-weight-bold">{{ __('Add a New Task') }}</div>
                <div class="card-body px-5">

                    <form action="{{ URL::action('TaskController@store') }}" method="POST">
                        {{ csrf_field() }}

                        <div class="form-group row">
                            <label for="id_u">User</label>
                            <select class="form-control" name="id_u">
                                @foreach ($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }} {{ $user->surname }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group row">
                            <label for="id_p">Project</label>
                            <select class="form-control" name="id_p">
                                @foreach ($projects as $project)
                                <option value="{{ $project->id }}">{{ $project->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="row justify-content-center">
                            <input class="btn btn-water" type="submit" value="Add Task">
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection