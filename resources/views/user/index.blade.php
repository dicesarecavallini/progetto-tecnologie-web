@extends('layouts.app')

@section('head')
<title>Users</title>
@endsection

@section('content')
<div class="container pt-2">

    <ul class="navbar-nav mr-auto">
        <li class="nav-item my-1">
            <a class="btn btn-water" href="{{ URL::action('UserController@create') }}">New Employee</a>
        </li>
    </ul>

    <br>

    <div class="row justify-content-center">


        <table class="table">
            <thead>
                <tr class="bg-cloud">
                    <th scope="col">Type</th>
                    <th scope="col">Name</th>
                    <th scope="col">eMail</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @if (count($users) > 0)
                @foreach ($users as $user)
                @if($user->is_admin)
                <tr class="bg-water-45">
                    <td>Admin</td>
                    @else
                <tr class="bg-cloud">
                    <td>User</td>
                    @endif
                    <td>{{ $user->name }} {{ $user->surname }}</td>
                    <td><a class="text-black u" href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                    
                    <td>
                        <a class="btn btn-sm btn-water text-white mx-2 showUserModal" id="show{{ $user->id }}" name="{{ $user->id }}" data-toggle="modal" data-target="#showUserModal{{ $user->id }}"> Tasks </a>
                        <a class="btn btn-danger text-white btn-sm mx-1" data-toggle="modal" data-target="#deleteUserModal{{ $user->id }}">Delete</a>
                    </td>
                    <!-- modal warning delete -->
                    <div class="modal fade" id="deleteUserModal{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteUserModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-danger">
                                    <h5 class="modal-title strong text-center text-white" id="showUserModalLabel">{{ $user->name }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body text-center">
                                    <div id="deleteModal{{ $user->id }}">
                                        <strong>Are you sure you want to delete {{ $user->name }} {{ $user->surname }}?</strong>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <a class="btn btn-danger" href="{{ URL::action('UserController@destroy', $user->id) }}" data-id="{{ $user->id }}"> Delete </a>
                                    <button type="button" class="btn btn-water" data-dismiss="modal"> Cancel </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- modal show -->
                    <div class="modal fade" name="{{ $user->id }}" id="showUserModal{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="showUserModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-water">
                                    <h5 class="modal-title strong text-center text-white" id="showUserModalLabel">{{ $user->name }} {{ $user->surname }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body text-center">
                                    <div id="showModal{{ $user->id }}">
                                        <!-- ajax -->
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-water" data-dismiss="modal"> Close </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </tr>
                @endforeach
                @else
                <p>No Users...</p>
                @endif
            </tbody>
        </table>
    </div>
    @endsection

    @section('script')
    <script>
        $(document).ready(function() {

            var _token = $('input[name="_token"]').val();

            function show_user_tasks(id = '') {
                $.ajax({
                    url: "http://localhost:8000/task/" + id,
                    method: "GET",
                    data: {
                        id: id,
                        _token: _token
                    },
                    dataType: "json",
                    success: function(data) {
                        var output = '';
                        var i = 0;
                        console.log(data);

                        output += '<table class="table table-sm">';
                        output += '<thead><tr>';
                        output += '<th scope="col">Project</th>';
                        output += '<th scope="col">Hours Completed</th>';
                        output += '</tr></thead>';
                        output += '<tbody>';

                        while (data[i] != null) {
                            output += '<tr>';
                            output += '<td>' + data[i].proj_name + '</td>';
                            output += '<td>' + data[i].hours + '</td>';
                            output += '</tr>';
                            i++;
                        }

                        if (i == 0) {
                            output += '<div class="row"><p class="ml-3">No tasks to do currently...</p></div>'
                        }

                        output += '</div></tbody></table>';

                        $('#showModal' + id).html(output);
                    }
                })
            }

            $('.showUserModal').click(function() {
                var id = $(this).attr('name');
                console.log(id);
                show_user_tasks(id);
            });

        });
    </script>
    @endsection