@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      <a class="btn btn-water my-1" href="{{ URL::action('UserController@index') }}" role="button">Back to all Employees</a>
    </div>

    <div class="col-md-6">
      <div class="card border-grey">
        <div class="card-header bg-air font-weight-bold">{{ __('Add a New Employee') }}</div>

        <div class="card-body">
          <form action="{{ URL::action('UserController@store') }}" method="POST">
            {{ csrf_field() }}

            <div class="row">
              <div class="col-12 text-center mb-3">
                <div class="form-check form-check-inline mx-5">
                  <input class="form-check-input @error('is_admin') is-invalid @enderror" type="radio" name="is_admin" id="is_admin1" required autocomplete="is_admin" value="1" {{ old('is_admin')=="1" ? 'checked='.'"'.'checked'.'"' : '' }}>
                  @error('is_admin')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                  <label class="form-check-label" for="is_admin1">Admin</label>
                </div>
                <div class="form-check form-check-inline mx-5">
                  <input class="form-check-input @error('is_admin') is-invalid @enderror" type="radio" name="is_admin" id="is_admin0" required autocomplete="is_admin" value="0" {{ old('is_admin')=="0" ? 'checked='.'"'.'checked'.'"' : '' }}>
                  @error('is_admin')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                  <label class="form-check-label" for="is_admin0">User</label>
                </div>
              </div>
            </div>

            <div class="form-group row">
              <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

              <div class="col-md-6">
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                @error('name')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="surname" class="col-md-4 col-form-label text-md-right">{{ __('Surname') }}</label>

              <div class="col-md-6">
                <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ old('surname') }}" required autocomplete="surname" autofocus>

                @error('surname')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

              <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

              <div class="col-md-6">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

              <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
              </div>
            </div>

            <div class="row justify-content-center">
              <input class="btn btn-water" type="submit" value="Add Employee">
            </div>
            
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection