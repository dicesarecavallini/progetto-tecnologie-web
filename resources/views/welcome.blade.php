<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Athena</title>

  <!-- Fonts -->
  <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/cover/">
  <link href="https://fonts.googleapis.com/css?family=Istok+Web&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="/css/bootstrap.min.css" crossorigin="anonymous">
  <link rel="stylesheet" href="/css/cover.css" crossorigin="anonymous">
  <style>
    .cover-heading {
      font-size: 3.5rem;
    }
  </style>
</head>

<body>
  <div class="col-12 text-center">
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
      <header class="masthead">
        <div class="inner">

        </div>
      </header>

      <main role="main" class="inner cover my-auto">
        <!--komorebi-->
        <h1 class="cover-heading">Athena</h1>
        <h2 class="lead font-weight-bolder">Here to make your projects come true.</h2>
        <br>
        <p class="lead">
          @if (Route::has('login'))
          @auth
          <a class="btn btn-lg btn-secondary" href="{{ url('/home') }}">Activities</a>
          @else
          <a class="btn btn-lg btn-secondary" href="{{ route('login') }}">Login</a>
          @endauth
          @endif
        </p>
      </main>
    </div>
  </div>
</body>

</html>