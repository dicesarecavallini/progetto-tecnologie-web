@extends('layouts.app')

@section('head')
@endsection

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-3">
            @if (Auth::user()->is_admin == 1)
                <a class="btn btn-water" href="{{ URL::action('DateRangeController@index') }}" role="button">Back to Diary</a>
            @else
                <a class="btn btn-water" href="{{ url('home') }}" role="button">Back to Home</a>
            @endif
        </div>

        <div class="col-md-6">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="card border-grey">
                <div class="card-header bg-air font-weight-bold">{{ __('Add Hours') }}</div>

                <div class="card-body px-5">

                    <form action="{{ URL::action('CardController@add_hours') }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}

                        <div class="form-group row">
                            <label for="date_hours">Date</label>
                            <input class="form-control" type="date" name="date_hours" value="{{ $date }}" />
                        </div>

                        <div class="form-group row">
                            <label for="id_pro">Project</label>
                            <select class="form-control" name="id_pro">
                                @foreach ($projects as $project)
                                <option value="{{ $project->id }}">{{ $project->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group row">
                            <label for="hours">Add Hours</label>
                            <input class="form-control" type="number" name="hours"  value="{{ old('hours') }}"/>
                        </div>

                        <div class="form-group row">
                            <label for="notes_added">Notes</label>
                            <input class="form-control" type="text" name="notes_added" value="{{ old('notes_added') }}" />
                        </div>

                        <div class="row justify-content-center">
                            <input class="btn btn-water" type="submit" value="Save">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection