@extends('layouts.app')

@section('head')
<title>Project</title>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <a class="btn btn-water" href="{{ URL::action('ProjectController@index') }}" role="button">Back</a>
        </div>

        <div class="col-md-6">

            <div class="card border-info">
                <div class="card-header text-center bg-air">
                    <h5 class="card-title">{{ $project->name }}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">{{ $project->client->business_name }}</h6>
                </div>
                <div class="card-body text-center">
                    <p class="card-text">{{ $project->description }}</p>
                    <p class="card-text"><strong>Notes:</strong>
                        @if($project->notes)
                        {{ $project->notes }}
                        @else
                        None.
                        @endif
                    </p>
                    <p class="card-text"><strong>Period:</strong> {{ $project->start_date }} - {{ $project->end_date }}</p>
                    <p class="card-text"><strong>Hourly Cost:</strong> {{ $project->hourly_cost }}€/hour</p>
                    <hr>
                    <p><strong>Total hours:</strong> {{ $hours }}</p>
                    <p><strong>Users working on it:</strong></p>
                    
                        @foreach ($users as $user)
                        <p> {{ $user->name }} {{ $user->surname }} - Hours completed: {{ $user->hours_completed}} </p>
                        @endforeach
                        @if(count($users) == 0)
                        <p>No one.</p>
                        @endif
                    
                    <hr>
                    <div class="row justify-content-center">
                        <a class="btn btn-secondary mx-2" href="{{  URL::action('ProjectController@edit', $project->id) }}">Edit</a>
                        <a class="btn btn-danger text-white mx-2" data-toggle="modal" data-target="#deleteProjectModal{{ $project->id }}">Delete</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- modal warning delete -->
        <div class="modal fade" id="deleteProjectModal{{ $project->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteProjectModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h5 class="modal-title strong text-center text-white" id="showProjectModalLabel">{{ $project->name }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <div id="showModal{{ $project->id }}">
                            <strong>Are you sure you want to delete {{ $project->name }}?</strong>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-danger" href="{{ URL::action('ProjectController@destroy', $project->id) }}" data-id="{{ $project->id }}"> Delete </a>
                        <button type="button" class="btn btn-water" data-dismiss="modal"> Cancel </button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<!--
<div class="col-md-12">
    <p>id: {{ $project->id }}</p>
    <p>name: {{ $project->name }} </p>
    <p>Client: {{ $project->client->business_name }}</p>
    <p>Total hours: {{ $hours }}</p>
    <p>Users</p>
    <ul>
        @foreach ($users as $user)
        <li> {{ $user->name }} {{ $user->surname }} {{ $user->email }} ({{ $user->hours_completed}}) </li>
        @endforeach
    </ul>

</div>

<div class="row">
    <div class="col-md-12">
        <a class="btn btn-secondary" href="{{  URL::action('ProjectController@edit', $project->id) }}">Edit</a>
        <a class="btn btn-danger" href="#">delete</a>
    </div>
</div>-->
@endsection