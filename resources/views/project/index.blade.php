@extends('layouts.app')

@section('head')
@endsection

@section('content')

<div class="container pt-2">

    <div class="row">
        <div class="col-md-2 ml-3">
            <a class="btn btn-water" href="{{ URL::action('ProjectController@create') }}">New Project</a>
        </div>
    </div>

    <div class="row mx-auto">
        @if (count($projects) > 0)
        @foreach ($projects as $pro)

        <div class="card border-info mb-3 mx-3 my-3 pb-4 px-0" style="width: 15rem;">
            <div class="card-header text-center bg-air">{{ $pro->client->business_name }}</div>
            <div class="card-body text-info">
                <h5 class="card-title text-center">{{ $pro->name }}</h5>
                <p class="card-text text-center text-dark">{{ $pro->description }}</p>
                <hr>
                <div class="row bottom-fixed px-auto">
                    <div class="col mt-3 px-auto">
                        <a class="btn btn-water btn-sm mx-1" href="{{ URL::action('ProjectController@show', $pro->id) }}"> Show </a>
                        <a class="btn btn-cloud btn-sm mx-1" href="{{ URL::action('ProjectController@edit', $pro->id) }}">Edit</a>
                        <a class="btn btn-danger text-white btn-sm mx-1" data-toggle="modal" data-target="#deleteProjectModal{{ $pro->id }}">Delete</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- modal warning delete -->
        <div class="modal fade" id="deleteProjectModal{{ $pro->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteProjectModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h5 class="modal-title strong text-center text-white" id="showProjectModalLabel">{{ $pro->name }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <div id="showModal{{ $pro->id }}">
                            <strong>Are you sure you want to delete {{ $pro->name }}?</strong>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-danger" href="{{ URL::action('ProjectController@destroy', $pro->id) }}" data-id="{{ $pro->id }}"> Delete </a>
                        <button type="button" class="btn btn-water" data-dismiss="modal"> Cancel </button>
                    </div>
                </div>
            </div>
        </div>

        @endforeach
        @else
        <p>No projects</p>
        @endif

    </div>

</div>
@endsection