@extends('layouts.app')

@section('head')
@endsection

@section('content')
<div class="container">
  <div class="row">

    <div class="col-md-3">
      <a class="btn btn-water my-1" href="{{ URL::action('ProjectController@index') }}" role="button">Back to all Projects</a>
    </div>

    <div class="col-md-6">

      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="card border-grey">
        <div class="card-header bg-air font-weight-bold">{{ __('Add a New Project') }}</div>

        <div class="card-body px-5">

          <form action="{{ URL::action('ProjectController@store') }}" method="POST">
            {{ csrf_field() }}

            <div class="form-group row">
              <label for="id_c">Client Name</label>
              <select class="form-control" name="id_c">
                @foreach ($clients as $client)
                <option value="{{ $client->id }}">{{ $client->business_name }}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group row">
              <label for="name">Project Name</label>
              <input class="form-control" type="text" name="name" value="{{ old('name') }}" />
            </div>

            <div class="form-group row">
              <label for="description">Description</label>
              <input class="form-control" type="text" name="description" value="{{ old('description') }}" />
            </div>

            <div class="form-group row">
              <label for="notes">Notes</label>
              <input class="form-control" type="text" name="notes" value="{{ old('notes') }}" />
            </div>

            <div class="form-group row">
              <label for="start_date">Start Date</label>
              <input class="form-control" type="date" name="start_date" value="{{ old('start_date') }}" />
            </div>

            <div class="form-group row">
              <label for="end_date">End Date</label>
              <input class="form-control" type="date" name="end_date" value="{{ old('end_date') }}" />
            </div>

            <div class="form-group row">
              <label for="hourly_cost">Hourly cost</label>
              <input class="form-control" type="number" name="hourly_cost" value="{{ old('hourly_cost') }}" />
            </div>

            <div class="row justify-content-center">
              <input class="btn btn-water" type="submit" value="Add Project">
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection