<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Athena') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-datepicker.css') }}" rel="stylesheet">

    @yield('script')

</head>
<style>
    body {
        background-color: #EBEBEB !important;
    }

    .u {
        text-decoration: underline;
    }

    .font-500{
        font-weight: 500;
    }

    .underlined,
    .underlined:hover,
    .underlined:focus {
        border-bottom: solid 2px;
    }

    .bg-cloud {
        background-color: #C6CAD3 !important;
    }

    .bg-cloud-60 {
        background-color: rgba(198,202,211,0.75) !important;
    }

    .bg-air {
        background-color: #90B6CB !important;
    }

    .bg-air-40 {
        background-color: rgba(144, 182, 203, 0.40) !important;
    }

    .bg-lightwater {
        background-color: #57A6C4 !important;
    }

    .bg-lightwater-40 {
        background-color: rgba(87, 166, 196, 0.40) !important;
    }

    .bg-water {
        background-color: #2F84A9 !important;
    }

    .bg-water-40 {
        background-color: rgba(47, 132, 169, 0.35) !important;
    }

    .bg-water-45 {
        background-color: rgba(47, 132, 169, 0.45) !important;
    }

    .bg-white-40 {
        background-color: rgba(243,244,246,0.10) !important;
    }

    .btn-cloud {
        color: black;
        background-color: #C6CAD3 !important;
        border-color: #9EA1A8 !important;
    }

    .btn-cloud:hover {
        color: black;
        background-color: #9EA1A8 !important;
        border-color: #9EA1A8 !important;
    }

    .btn-air {
        color: #fff;
        background-color: #90B6CB !important;
        border-color: #38424E !important;
    }

    .btn-air:hover {
        color: #fff;
        background-color: #3e6f8a !important;
        border-color: #38424E !important;
    }

    .btn-water {
        color: #fff;
        background-color: #2F84A9 !important;
        border-color: #256987 !important;
    }

    .btn-water:hover {
        color: #fff;
        background-color: #205C76 !important;
        border-color: #164052 !important;
    }

    .btn-outline-water {
        color: #2F84A9;
        background-color: #FAF9F6 !important;
        border-color: #2F84A9 !important;

    }

    .btn-outline-water:hover {
        color: #FAF9F6;
        background-color: #4390B1 !important;
        border-color: #2F84A9 !important;
    }

    .border-grey {
        border-color: #6c757d !important;
    }

    .border-air {
        border-color: #90B6CB !important;
    }

    .border-black {
        border-color: #000 !important;
    }

    .link-water {
        color: #2F84A9;
        background-color: #fff !important;
        border-color: #fff !important;
    }

    .cliccable {
        cursor: pointer;
    }

    .bottom-fixed {
        position: absolute;
        bottom: 15px;
    }

    .text-black {
        color: black;
    }
</style>

<body>
    <div id="app">
        <nav class="navbar fixed-top navbar-expand-md navbar-dark bg-dark shadow-sm">
            <div class="container">
                @if( basename($_SERVER['PHP_SELF']) == 'home' )
                <a class="navbar-brand underlined pb-0 my-auto" href="{{ url('/home') }}">
                    {{ config('app.name', 'Athena') }}
                </a>
                @else
                <a class="navbar-brand" href="{{ url('/home') }}">
                    {{ config('app.name', 'Athena') }}
                </a>
                @endif
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <!-- SOLO SE ADMIN -->
                    @if (Auth::user()->is_admin == 1)
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item mx-2">
                            @if (class_basename(Route::current()->controller) == 'ClientController')
                            <a class="nav-link underlined pb-0 px-0" href="{{ URL::action('ClientController@index') }}">{{ __('Clients') }}</a>
                            @else
                            <a class="nav-link" href="{{ URL::action('ClientController@index') }}">{{ __('Clients') }}</a>
                            @endif
                        </li>
                        <li class="nav-item mx-2">
                            @if (class_basename(Route::current()->controller) == 'ProjectController')
                            <a class="nav-link underlined pb-0 px-0" href="{{ URL::action('ProjectController@index') }}">{{ __('Projects') }}</a>
                            @else
                            <a class="nav-link" href="{{ URL::action('ProjectController@index') }}">{{ __('Projects') }}</a>
                            @endif
                        </li>
                        <li class="nav-item mx-2">
                            @if (class_basename(Route::current()->controller) == 'UserController')
                            <a class="nav-link underlined pb-0 px-0" href="{{ URL::action('UserController@index') }}">{{ __('Employees') }}</a>
                            @else
                            <a class="nav-link" href="{{ URL::action('UserController@index') }}">{{ __('Employees') }}</a>
                            @endif
                        </li>
                        <li class="nav-item mx-2">
                            @if (class_basename(Route::current()->controller) == 'TaskController')
                            <a class="nav-link underlined pb-0 px-0" href="{{ URL::action('TaskController@index') }}">{{ __('Tasks') }}</a>
                            @else
                            <a class="nav-link" href="{{ URL::action('TaskController@index') }}">{{ __('Tasks') }}</a>
                            @endif
                        </li>
                        <li class="nav-item dropdown">
                            @if (class_basename(Route::current()->controller) == 'StatsController')
                            <a class="nav-link underlined pb-0 px-0 dropdown-toggle" href="#" id="dropdownStats" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Stats
                            </a>
                            @else
                            <a class="nav-link dropdown-toggle pl-2" href="#" id="dropdownStats" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Stats
                            </a>
                            @endif
                            <div class="dropdown-menu" aria-labelledby="dropdownStats">
                                <a class="dropdown-item" href="{{ URL::action('DateRangeController@index_project')}}">Projects</a>
                                <a class="dropdown-item" href="{{ URL::action('DateRangeController@index_client')}}">Clients</a>
                            </div>
                        </li>
                    </ul>
                    @endif
                    <!-- FIN QUI -->

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item mx-2">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        <!-- @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif -->
                        @else
                        @if (Auth::user()->is_admin == 1)
                            <li class="nav-item mx-2">
                                <a class="nav-link" href="{{ URL::action('DateRangeController@index')}}">My Diary</a>
                            </li>
                        @endif
                        <li class="nav-item mx-2 dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4 my-5">
            @yield('content')
        </main>
    </div>
</body>

</html>