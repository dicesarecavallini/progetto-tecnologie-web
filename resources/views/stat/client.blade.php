@extends('layouts.datepicker')

@section('head')
@endsection

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row mt-2 mb-4 align-items-center">
                <div class="col-md-5 my-auto"><strong>Total Records: </strong><span id="total_records"></span></b></div>
                <div class="col-md-5 my-auto">
                    <div class="input-group input-daterange align-items-center">
                        <div class="input-group-addon mx-2"><p clasS="my-auto">Select range: </p></div>
                        <input type="text" name="from_date" id="from_date" readonly class="form-control" />
                        <div class="input-group-addon mx-2">&mdash;</div>
                        <input type="text" name="to_date" id="to_date" readonly class="form-control" />
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="button" name="filter" id="filter" class="btn btn-water btn-sm my-1">Filter</button>
                    <button type="button" name="refresh" id="refresh" class="btn btn-cloud btn-sm my-1">Refresh</button>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th width="35%" class="border-air bg-water-40">Client</th>
                            <th width="50%" class="border-air bg-water-40">Total Hours</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
                {{ csrf_field() }}
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function() {

        var date = new Date();

        $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        var _token = $('input[name="_token"]').val();

        fetch_data_client();

        function fetch_data_client(from_date = '', to_date = '') {
            $.ajax({
                url: "{{ route('daterange.fetch_data_client') }}",
                method: "POST",
                data: {
                    from_date: from_date,
                    to_date: to_date,
                    _token: _token
                },
                dataType: "json",
                success: function(data) {
                    var output = '';
                    $('#total_records').text(data.length);
                    for (var count = 0; count < data.length; count++) {
                        output += '<tr>';
                        output += '<td>' + data[count].business_name + '</td>';
                        output += '<td>' + data[count].hours + '</td>';
                    }
                    $('tbody').html(output);
                }
            })
        }

        $('#filter').click(function() {
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if (from_date != '' && to_date != '') {
                fetch_data_client(from_date, to_date);
            } else {
                alert('Both Date is required');
            }
        });

        $('#refresh').click(function() {
            $('#from_date').val('');
            $('#to_date').val('');
            fetch_data_client();
        });


    });
</script>
@endsection