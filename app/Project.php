<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['id_c', 'name', 'description', 'notes', 'start_date', 'end_date', 'hourly_cost'];

    public function tasks() {
        return $this->hasMany('App\Task');
    }

    public function traces() {
        return $this->hasMany('App\Trace');
    }
    
    public function client() {
        return $this->belongsTo('App\Client', 'id_c');
    }
}
