<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['business_name', 'referent_name', 'business_surname', 'referent_email', 'SSID', 'PEC', 'P_IVA', 'logo', 'slogan'];
    //protected $guarded = [];

    public function projects() {
        return $this->hasMany('App\Project');
    }
}
