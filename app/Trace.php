<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trace extends Model
{
    protected $fillable = ['insert_day', 'hours_added', 'id_p', 'notes'];
    
    public function project() {
        return $this->belongsTo('App\Project', 'id_p');
    }
}
