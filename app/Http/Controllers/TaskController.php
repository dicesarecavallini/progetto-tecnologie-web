<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Project;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::all();
        $projects = Project::all();
        $users = User::all();

        return view('task.index',['tasks'=>$tasks,'users'=>$users,'projects'=>$projects]);
    }

    public function indexUserTasks(Request $request)
    {
        $data = DB::table('tasks')
            ->where('id_u', '=', $request->id)
            ->join('users', 'users.id', '=', 'tasks.id_u')
            ->join('projects','projects.id','=','tasks.id_p')
            ->select('projects.id as id','projects.name as proj_name','tasks.hours_completed as hours')
            ->get();

        echo json_encode($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = Project::all();
        $users = User::all();


        return view('task.create', compact('projects'), compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'id_u' => 'required|exists:users,id',
            'id_p' => 'required|exists:projects,id',
        ]);

        if ($validator->fails()) {
            return redirect('task/index')
                ->withErrors($validator)
                ->withInput();
        }

        $added = Task::create($input);

        $data = DB::table('tasks as t')
            ->join('users as u', 'u.id', '=', 't.id_u')
            ->join('projects as p', 'p.id', '=', 't.id_p')
            ->select('t.id as id', 'u.name as user_name','u.surname as user_surname', 'p.name as project_name', 't.hours_completed as hour_completed','p.hourly_cost as hourly_cost')
            ->where('t.id', '=', $added->id)
            ->get();

        echo json_encode($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);
        $task->delete();

        return json_encode( ['message' => 'ok'] );
    }
}