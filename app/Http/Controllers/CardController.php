<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Trace;

class CardController extends Controller
{
    public function insert() {
        $user = auth()->user()->id;

        $projects = DB::table('tasks')
            ->join('projects', 'projects.id', '=', 'tasks.id_p')
            ->where('tasks.id_u', '=', $user)
            ->select('projects.id', 'projects.name')
            ->get();
        
        $date = Carbon::now()->toDateString();

        return view('card.insert', compact('projects'), compact('date'));
    }

    public function add_hours(Request $request) {

        $user = auth()->user()->id;

        $input = $request->all();

        $validator = Validator::make($input, [
            'date_hours' => 'required|date',
            'id_pro' =>'required|exists:projects,id',
            'hours' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect('addcard')
                ->withErrors($validator)
                ->withInput();
        }


        DB::table('tasks')
            ->join('projects', 'projects.id', '=', 'tasks.id_p')
            ->join('users', 'users.id', '=', 'tasks.id_u')
            ->where('id_u', '=', $user)
            ->where('id_p', '=', $input['id_pro'])
            ->increment('tasks.hours_completed', $input['hours']);

        $newtrace = new Trace;
        $newtrace -> insert_day = $input['date_hours'];
        $newtrace -> hours_added = $input['hours'];
        $newtrace -> id_p = $input['id_pro'];
        $newtrace -> notes = $input['notes_added'];
        $newtrace -> save();

        return redirect("/mydiary");
    }
    
}
