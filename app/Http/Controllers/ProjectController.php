<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Client;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();

        return view('project.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::all();

        return view('project.create', compact('clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        //dd($request);
        $validator = Validator::make($input, [
            'id_c' => 'required|exists:clients,id',
            'name' => 'required',
            'description' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
            'hourly_cost' => 'required|numeric|max:200',
        ]);

        //dd($input);

        if ($validator->fails()) {
            return redirect('project/create')
                ->withErrors($validator)
                ->withInput();
        }

        Project::create($input);
        
        return redirect('/project');
        //return view('project.show');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function show($id)
    {
        $project = Project::find($id);

        $hours = DB::table('tasks')
            ->join('projects', 'tasks.id_p', '=', 'projects.id')
            ->where('id_p', '=', $id)
            ->sum('tasks.hours_completed');

        $users = DB::table('users')
            ->join('tasks', 'users.id', '=', 'tasks.id_u')
            ->select('users.name', 'users.surname', 'users.email', 'tasks.hours_completed')
            ->where('tasks.id_p', '=', $id)
            ->get();
            
        return view('project.show', compact('project', 'hours', 'users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clients = Client::all();
        $project = Project::find($id);

        return view('project.edit', compact('clients', 'project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'id_c' => 'required|exists:clients,id',
            'name' => 'required',
            'description' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
            'hourly_cost' => 'required|numeric|max:200',
        ]);

        if ($validator->fails()) {
            return redirect('project/edit')
                ->withErrors($validator)
                ->withInput();
        }

        $project = Project::find($id);
        $project->update($input);

        return redirect("/project"); // Show
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);
        $project->delete();

        return redirect('/project');
    }
}
