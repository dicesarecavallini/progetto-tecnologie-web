<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function adminHome()
    {
        $projects = DB::table('projects')
            ->orderBy('created_at', 'desc')
            ->select('id', 'name', 'created_at')
            ->take(3)
            ->get();

        $noTasks =  DB::table("users")->select('users.name', 'users.surname')->whereNotIn('users.id',function($query) {

                $query->select('id_u')->from('tasks');
             
            })->get();
        
        if(count($noTasks) == 0) {

            $users = DB::table('tasks')
                ->join('users', 'users.id', '=', 'tasks.id_u')
                ->select('users.name','users.surname', DB::raw('COUNT(tasks.id) as n'))
                ->groupBy('tasks.id_u')
                ->orderBy('n', 'asc')
                ->take(3)
                ->get();

            return view('adminHome', compact('projects'), compact('users', 'noTasks'));

        } else if(count($noTasks) == 1) {

            $users = DB::table('tasks')
                ->join('users', 'users.id', '=', 'tasks.id_u')
                ->select('users.name','users.surname', DB::raw('COUNT(tasks.id) as n'))
                ->groupBy('tasks.id_u')
                ->orderBy('n', 'asc')
                ->take(2)
                ->get();

            return view('adminHome', compact('projects'), compact('noTasks', 'users'));

        } else if(count($noTasks) == 2) {

            $users = DB::table('tasks')
                ->join('users', 'users.id', '=', 'tasks.id_u')
                ->select('users.name','users.surname', DB::raw('COUNT(tasks.id) as n'))
                ->groupBy('tasks.id_u')
                ->orderBy('n', 'asc')
                ->take(1)
                ->get();

            return view('adminHome', compact('projects'), compact('noTasks', 'users'));
            
        } else if(count($noTasks) >= 3) {

            $users = [];
            $noTasks =  DB::table("users")->select('users.name', 'users.surname')->whereNotIn('users.id',function($query) {

                $query->select('id_u')->from('tasks');
             
            })
            ->take(3)
            ->get();
            
            return view('adminHome', compact('projects'), compact('noTasks', 'users'));

        }
        
    }

    public function clients()
    {
        return view('client');
    }

    public function projects()
    {
        return view('project');
    }

    public function employees()
    {
        return view('user');
    }

    public function diary()
    {
        return view('indexMyTasks');
    }

    public function activities()
    {
        return view('task');
    }
}
