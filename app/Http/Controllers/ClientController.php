<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = DB::table('clients')
        ->select('id','business_name','logo','slogan')
        ->get();

        return view('client.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);

        $input = $request->all(); 

        $validator = Validator::make($input, [
            'business_name' => 'required',
            'referent_name' => 'required',
            'business_surname' => 'required',
            'referent_email' => 'required|email',
            'SSID' => 'required',
            'PEC' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect('client/create')
                ->withErrors($validator)
                ->withInput();
        }

        //dd($input);

        Client::create($input);
        
        return redirect('/client');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response 
     */
    public function show(Request $request)
    {
        $data = DB::table('clients')
            ->where('id', '=', $request->id)
            ->get();
        
        echo json_encode($data);

        /*$client = Client::find($id);
        
        return view('client.show', compact('client'));*/
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // I CLIENTI NON SI POSSONO MODIFICARE IN TEORIA
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);
        $client->delete();
        
        return redirect('/client');
    }

    function order_by(Request $request){
        $data = DB::table('tasks')
            ->order_by($request->order)
            ->get();
        
        echo json_encode($data);
    }
}
