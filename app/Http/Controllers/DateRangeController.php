<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Project;
use App\User;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DateRangeController extends Controller
{
    public function indexHome()
    {
        $date = Carbon::now();
        $y = $date->year;
        $m = $date->month;
        if ($m < 10) {
            $y .= "-0";
            $y .= $m;
        } else {
            $y .= "-";
            $y .= $m;
        }
;       return view('task.diary', compact('y'));
    }

    public function index()
    {
        $date = Carbon::now();
        $y = $date->year;
        $m = $date->month;
        if ($m < 10) {
            $y .= "-0";
            $y .= $m;
        } else {
            $y .= "-";
            $y .= $m;
        }
;       return view('task.diary', compact('y'));
    }

    public function fetch_data(Request $request)
    {
        $user = auth()->user()->id;

        if($request->ajax()) {
            if($request->from_date != '') {

                $a = $request->from_date;
                $month = substr($a, -2);
                $year = substr($a, 0, -3);
                
                $data = DB::table('tasks')
                    ->join('users', 'users.id', '=', 'tasks.id_u')
                    ->join('projects', 'projects.id', '=', 'tasks.id_p')
                    ->where(function ($query) use ($month, $year) {
                        $query->where(function ($query) use ($month, $year) {
                            $query->whereMonth('projects.start_date', '<=', $month)->whereYear('projects.start_date', '=', $year);
                        })
                        ->orWhere(function ($query) use ($year) {
                            $query->whereYear('projects.start_date', '<', $year);
                        });
                    })
                    ->where(function ($query) use ($month, $year) {
                        $query->where(function ($query) use ($month, $year) {
                            $query->whereMonth('projects.end_date', '>=', $month)->whereYear('projects.end_date', '=', $year);
                        })
                        ->orWhere(function ($query) use ($year) {
                            $query->whereYear('projects.end_date', '>', $year);
                        });
                    })
                    ->where('id_u', '=', $user)
                    ->select('projects.id','projects.name as name', 'tasks.hours_completed as hours','projects.hourly_cost as hourly_cost', 'projects.start_date as start_date', 'projects.end_date as end_date')
                    ->get();
            
            } else {
                $date = Carbon::now();
                $y = $date->year;
                $m = $date->month;
                if ($m < 10) {
                    $y .= "-0";
                    $y .= $m;
                } else {
                    $y .= "-";
                    $y .= $m;
                }

                $data = DB::table('tasks')
                    ->join('users', 'users.id', '=', 'tasks.id_u')
                    ->join('projects', 'projects.id', '=', 'tasks.id_p')
                    ->where(function ($query) use ($m, $y) {
                        $query->where(function ($query) use ($m, $y) {
                            $query->whereMonth('projects.start_date', '<=', $m)->whereYear('projects.start_date', '=', $y);
                        })
                        ->orWhere(function ($query) use ($y) {
                            $query->whereYear('projects.start_date', '<', $y);
                        });
                    })
                    ->where(function ($query) use ($m, $y) {
                        $query->where(function ($query) use ($m, $y) {
                            $query->whereMonth('projects.end_date', '>=', $m)->whereYear('projects.end_date', '=', $y);
                        })
                        ->orWhere(function ($query) use ($y) {
                            $query->whereYear('projects.end_date', '>', $y);
                        });
                    })
                    ->where('id_u', '=', $user)
                    ->select('projects.id','projects.name as name', 'tasks.hours_completed as hours','projects.hourly_cost as hourly_cost', 'projects.start_date as start_date', 'projects.end_date as end_date')
                    ->get();
            }

            echo json_encode($data);
        }
    }

    

    public function index_project()
    {
        return view('stat.project');
    }

    public function fetch_data_project(Request $request)
    {
        $user = auth()->user()->id;
        
        if($request->ajax()) {
            if($request->from_date != '' && $request->to_date != '') {

                $data = DB::table('projects')
                    ->join('traces', 'traces.id_p', '=', 'projects.id')
                    ->whereBetween('traces.insert_day', array($request->from_date, $request->to_date))
                    ->select('projects.name',DB::raw('SUM(traces.hours_added) as hours'),'traces.id_p', DB::raw('SUM(traces.hours_added) * projects.hourly_cost as tot'))
                    ->groupBy('traces.id_p')
                    ->get();

            } else {
                $start = new Carbon('first day of this month');
                $end = new Carbon('last day of this month');

                $data = DB::table('projects')
                    ->join('traces', 'traces.id_p', '=', 'projects.id')
                    ->whereBetween('traces.insert_day', array($start, $end))
                    ->select('projects.name',DB::raw('SUM(traces.hours_added) as hours'),'traces.id_p', DB::raw('SUM(traces.hours_added) * projects.hourly_cost as tot'))
                    ->groupBy('traces.id_p')
                    ->get();

            }
            echo json_encode($data);
        }
    }

    public function index_client()
    {
        return view('stat.client');
    }

    public function fetch_data_client(Request $request)
    {
        $user = auth()->user()->id;
        
        if($request->ajax()) {
            if($request->from_date != '' && $request->to_date != '') {

                $data = DB::table('clients')
                    ->join('projects', 'projects.id_c', '=', 'clients.id')
                    ->join('traces', 'traces.id_p', '=', 'projects.id')
                    ->whereBetween('traces.insert_day', array($request->from_date, $request->to_date))
                    ->select('clients.business_name',DB::raw('SUM(traces.hours_added) as hours'),'projects.id_c')
                    ->groupBy('projects.id_c')
                    ->get();

            } else {
                $start = new Carbon('first day of this month');
                $end = new Carbon('last day of this month');

                $data = DB::table('clients')
                    ->join('projects', 'projects.id_c', '=', 'clients.id')
                    ->join('traces', 'traces.id_p', '=', 'projects.id')
                    ->whereBetween('traces.insert_day', array($start, $end))
                    ->select('clients.business_name',DB::raw('SUM(traces.hours_added) as hours'),'projects.id_c')
                    ->groupBy('projects.id_c')
                    ->get();

            }
            echo json_encode($data);
        }
    }
}
