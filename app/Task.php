<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['id_u', 'id_p', 'hours_completed'];

    public function user() {
        return $this->belongsTo('App\User', 'id_u');
    }

    public function project() {
        return $this->belongsTo('App\Project', 'id_p');
    }
}
