<?php

use Illuminate\Database\Seeder;
use App\Task;

class CreateTasksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $task = [
            [
                'hours_completed' => 0,
                'id_u' => 1,
                'id_p' => 1
            ],
            [
                'hours_completed' => 0,
                'id_u' => 1,
                'id_p' => 2
            ],
            [
                'hours_completed' => 0,
                'id_u' => 2,
                'id_p' => 1
            ]

        ];

        foreach ($task as $key => $value) {
            Task::create($value);
        }
    }
}
