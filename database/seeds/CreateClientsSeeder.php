<?php

use Illuminate\Database\Seeder;
use App\Client;

class CreateClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = [
            [
                'business_name'=>'ICOS',
                'referent_name'=>'Marco',
                'business_surname'=>'Rossi',
                'referent_email'=>'icos@icos.it',
                'SSID'=>'0024519348',
                'PEC'=>'icos@pec.icos.it',
                'P_IVA'=>'0000000000123456',
                'logo'=>'https://www.icos.it/images/icos.png',
                'slogan'=> 'Distribuzione a valore di soluzioni IT',
             ],
             [
                'business_name'=>'Penta',
                'referent_name'=>'Carlotta',
                'business_surname'=>'Bianchi',
                'referent_email'=>'info@pentasrl.it',
                'SSID'=>'1020425967',
                'PEC'=>'info@pec.pentasrl.it',
                'P_IVA'=>'0000000000181456',
                'logo'=>'http://pentasrl.net/wp-content/uploads/2016/10/Logo-PENTA-trasp.png',
                'slogan'=> "Isolanti e Sistemi per l'Edilizia",
             ],
             [
                'business_name'=>'BIA',
                'referent_name'=>'Daniele',
                'business_surname'=>'Verdi',
                'referent_email'=>'commerciale@biaitalia.it',
                'SSID'=>'2147853046',
                'PEC'=>'info@pec.biaitalia.it',
                'P_IVA'=>'000000086723456',
                'logo'=>'https://www.biacouscous.it/ita/wp-content/uploads/sites/5/2018/07/biacouscous-logo-2018-e1532082037366.png',
                'slogan'=> 'The CousCous Specialist',
             ],
             [
                'business_name'=>'Aviko',
                'referent_name'=>'Marta',
                'business_surname'=>'Giusti',
                'referent_email'=>'richieste@aviko.it',
                'SSID'=>'5204193401',
                'PEC'=>'info@pec.aviko.it',
                'P_IVA'=>'000000715343456',
                'logo'=>'https://www.aviko.it/images/favicon.ico?aviko',
                'slogan'=> 'Condividere la nostra passione per le patate',
             ],
             [
                'business_name'=>'FrigAir',
                'referent_name'=>'Esther',
                'business_surname'=>'Minguzzi',
                'referent_email'=>'info@frigair.com',
                'SSID'=>'5013408267',
                'PEC'=>'info@pec.frigair.it',
                'P_IVA'=>'0000000012746456',
                'logo'=>'https://www.frigair.com/assets/854836aa6481a2fd0cb664f7b0cffb14/logo_frigair.png',
                'slogan'=> 'Leader in the Automotive Aftermarket',
             ],
             [
                'business_name'=>'Ferri',
                'referent_name'=>'Antonio',
                'business_surname'=>'Cambiali',
                'referent_email'=>'info@ferrisrl.it',
                'SSID'=>'0218249105',
                'PEC'=>'ferrisl@legalmail.it',
                'P_IVA'=>'0000001862493456',
                'logo'=>'http://www.ferrisrl.it/www.ferrisrl.it/repository/1/ita-534.png',
                'slogan'=> 'Trinciatrici e Decespugliatrici idrauliche di classe superiore',
             ],
             [
                'business_name'=>'Tekno Tubi S.r.l.',
                'referent_name'=>'Ilaria',
                'business_surname'=>'Raggi',
                'referent_email'=>'info@interpumpgroup.it',
                'SSID'=>'1523482172',
                'PEC'=>'info@pec.interpumpgroup.it',
                'P_IVA'=>'0000000842615815',
                'logo'=>'https://www.interpumpgroup.it/download/immagini/fn150258.jpg',
                'slogan'=> 'Sagomatura e assemblaggio di tubi rigidi',
             ],
             [
                'business_name'=>'Niagara',
                'referent_name'=>'Matteo',
                'business_surname'=>'Verdazzi',
                'referent_email'=>'amministrazione@niagarapoggio.it',
                'SSID'=>'4256183245',
                'PEC'=>'niagara@registerpec.it',
                'P_IVA'=>'0000001762485517',
                'logo'=>'https://www.niagarapoggio.it/img/logo.png',
                'slogan'=> 'Trattamento Rifiuti Industriali',
             ],
             [
                'business_name'=>'Vortex Hydra',
                'referent_name'=>'Erika',
                'business_surname'=>'Tardossi',
                'referent_email'=>'vh.sales@vortexhydra.com',
                'SSID'=>'1527493186',
                'PEC'=>'vortexhydra@pec.vortexhydra.it',
                'P_IVA'=>'0000415294351862',
                'logo'=>'https://www.vortexhydra.com/wp-content/uploads/2019/04/VH_orizzontale_standard_small_since.png',
                'slogan'=> 'Innovation & Engineering',
             ],
             [
               'business_name'=>'Ottica Di Cesare',
               'referent_name'=>'Marco',
               'business_surname'=>'Di Cesare',
               'referent_email'=>'marco.dicesare@gmail.com',
               'SSID'=>'1523495126',
               'PEC'=>'info@otticadicesare.it',
               'P_IVA'=>'00929330397',
               'logo'=>'http://www.otticadicesare.it/wp-content/uploads/2018/03/LOGO.PNG-e1522063572105.jpeg',
               'slogan'=> 'Seeing is believing',
            ],
        ];

        foreach ($client as $key => $value) {
            Client::create($value);
     }
    }
}
