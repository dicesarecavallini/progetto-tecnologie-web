<?php

use Illuminate\Database\Seeder;
use App\Project;

class CreateProjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $project = [
            [
                'id_c' => '1',
                'name' => 'Nuovo Sistema di allarme',
                'description' => 'Realizzazione di un nuovo sistema di vigilanza per gli uffici',
                'notes' => '',
                'start_date' => '2019-10-15',
                'end_date' => '2020-02-15',
                'hourly_cost' => '10.00',
            ],
            [
                'id_c' => '2',
                'name' => 'Gestione mail',
                'description' => 'Realizzazione di un servizio di posta elettronica condiviso tra i colleghi',
                'notes' => '',
                'start_date' => '2019-11-01',
                'end_date' => '2020-05-01',
                'hourly_cost' => '12.50',
            ],
            [
                'id_c' => '3',
                'name' => 'Massive Monkey',
                'description' => 'New calculation software for small and medium-sized enterprises',
                'notes' => '',
                'start_date' => '2020-01-07',
                'end_date' => '2020-08-01',
                'hourly_cost' => '11.50',
            ],
            [
                'id_c' => '1',
                'name' => 'Safety First',
                'description' => 'New home security system',
                'notes' => '',
                'start_date' => '2019-10-01',
                'end_date' => '2020-04-01',
                'hourly_cost' => '13.00',
            ],
            [
                'id_c' => '10',
                'name' => 'Music Glasses',
                'description' => 'New glasses with audio output near the ear',
                'notes' => '',
                'start_date' => '2019-11-01',
                'end_date' => '2020-05-01',
                'hourly_cost' => '12.00',
            ],
            [
                'id_c' => '4',
                'name' => 'PurèPiù',
                'description' => 'Nuovo purè, ancora più puro e più bio',
                'notes' => '',
                'start_date' => '2019-03-01',
                'end_date' => '2020-08-01',
                'hourly_cost' => '9.00',
            ],
        ];

        foreach ($project as $key => $value) {
            Project::create($value);
        }
    }
}
