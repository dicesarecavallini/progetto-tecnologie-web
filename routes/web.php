<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Auth\Middleware\Authenticate;




Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home',         'DateRangeController@indexHome')->name('home')->middleware('is_normalUser');
Route::get('admin/home',    'HomeController@adminHome')->name('admin.home')->middleware('is_admin');

Route::get('/client',   'HomeController@clients')->name('clients')->middleware('is_admin');
Route::get('/project',  'HomeController@projects')->name('projects')->middleware('is_admin');
Route::get('/user',     'HomeController@employees')->name('employees')->middleware('is_admin');
Route::get('/mydiary',  'HomeController@diary')->name('diary');
Route::get('/task',     'HomeController@activities')->name('activities')->middleware('is_admin');

Route::get('project/create',        'ProjectController@create')->middleware('is_admin');
Route::post('/project',             'ProjectController@store');
Route::get('/project/{id}',         'ProjectController@show')->middleware('is_admin');
Route::get('/project',              'ProjectController@index')->middleware('is_admin');
Route::get('/project/{id}/delete',  'ProjectController@destroy')->middleware('is_admin');
Route::get('/project/{id}/edit',    'ProjectController@edit')->middleware('is_admin');
Route::patch('/project/{id}',       'ProjectController@update')->middleware('is_admin');

Route::get('task/create',       'TaskController@create')->middleware('is_admin');
Route::post('/task/store',      'TaskController@store');
Route::get('/task',             'TaskController@index')->middleware('is_admin');
Route::get('/task/{id}',        'TaskController@indexUserTasks')->middleware('is_admin');
Route::get('/task/{id}/delete', 'TaskController@destroy')->middleware('is_admin');

Route::get('user/create',       'UserController@create')->middleware('is_admin');
Route::post('/user',            'UserController@store');
Route::get('/user',             'UserController@index')->middleware('is_admin');
Route::get('/user/{id}/delete', 'UserController@destroy')->middleware('is_admin');

Route::get('client/create',         'ClientController@create')->middleware('is_admin');
Route::post('client',               'ClientController@store');
Route::get('/client/{id}',          'ClientController@show')->middleware('is_admin');
Route::get('/client',               'ClientController@index')->middleware('is_admin');
Route::get('/client/{id}/delete',   'ClientController@destroy')->middleware('is_admin');


Route::get('/mydiary',              'DateRangeController@index')->middleware('auth');
Route::post('/mydiary/fetch_data',  'DateRangeController@fetch_data')->name('daterange.fetch_data')->middleware('auth');

Route::get('/projects_stats',                       'DateRangeController@index_project')->middleware('auth')->middleware('is_admin');
Route::post('/projects_stats/fetch_data_project',   'DateRangeController@fetch_data_project')->name('daterange.fetch_data_project')->middleware('auth')->middleware('is_admin');

Route::get('/clients_stats',                    'DateRangeController@index_client')->middleware('auth')->middleware('is_admin');
Route::post('/clients_stats/fetch_data_client', 'DateRangeController@fetch_data_client')->name('daterange.fetch_data_client')->middleware('auth')->middleware('is_admin');

Route::get('/addcard',      'CardController@insert')->middleware('auth');
Route::patch('/addcard',    'CardController@add_hours')->middleware('auth');
